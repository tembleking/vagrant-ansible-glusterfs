# Vagrant + Ansible + GlusterFS

## Deploy

```
$ make
```
Vagrant will deploy 3 VirtualBox VMs and Ansible will configure GlusterFS in them

Once it's finished, you can do:

```
# mount -t glusterfs 192.168.2.10:/test1 /mnt
```

And start using the distributed filesystem.

## Clean up

```
$ make clean
```

Well... self-explanatory.

