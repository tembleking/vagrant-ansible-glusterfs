
deploy:
	vagrant up --parallel
	ansible-playbook playbook.yaml

clean:
	vagrant destroy -f
